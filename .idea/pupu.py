import json
from time import strftime, sleep

import requests
import re


def pupuMessage(productId):
    try:
        # 目标网址
        url = "https://j1.pupuapi.com/client/product/storeproduct/detail/" + productId
        # 请求头
        head = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36'
        }

        # 发送请求
        res = requests.get(url, headers=head)
        jsonshopping = json.loads(res.text)

        # 利用正则表达式
        # 商品名称
        name = jsonshopping['data']['name']

        # 商品价格
        spec = jsonshopping['data']['spec']

        # 商品当前价格
        price = jsonshopping['data']['price']
        price = str(int(price) / 100)

        # 得到商品市场价格
        market_price = jsonshopping['data']['market_price']
        market_price = str(int(market_price) / 100)

        # 显示商品所有的详细信息
        share_content = jsonshopping['data']['share_content']
        print("--------------" + name + "----------")
        print("规格：" + spec)
        print("价格：" + price)
        print("原价/折扣价：" + market_price + "/" + price)
        print("详细内容：" + share_content)
        print("\n\n--------------" + name + "的价格波动----------")
    except:
        url = "https://j1.pupuapi.com/client/product/storeproduct/detail/" + productId

        head = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36'
        }

        res = requests.get(url, headers=head)
        errmsg = re.findall(r'errmsg":"(.*?)"', res.text)[0]
        print(errmsg)

def now_price(productId):
    try:
        while (1):
            url = "https://j1.pupuapi.com/client/product/storeproduct/detail/" + productId

            head = {
                'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36'
            }
            res = requests.get(url, headers=head)
            price = re.findall(r'price":(.*?),', res.text)[0]
            #折扣价格
            price = str(int(price) / 100)

            nowTimeAndPrint = strftime('%Y' + '-' + '%m' + '-' + '%d' + ' %H:%M,价格为' + price)
            print(nowTimeAndPrint)
            # 用sleep方法来实现时间间隔，时间间隔2秒
            sleep(2)

    except:
        print("进程结束")

    # 运行程序
if __name__ == '__main__':
    productId = "deef1dd8-65ee-46bc-9e18-8cf1478a67e9/2a89a557-ff99-481d-a1a5-75098417c956"
    pupuMessage(productId)
    now_price(productId)


